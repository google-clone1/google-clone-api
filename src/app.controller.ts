import { Body, Controller, Get, Post } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @Get()
  getHello(): string {
    return this.appService.getHello() + new Date();
  }

  @Post('form')
  async submitForm(@Body() body: string[],) {
    return await this.appService.submitForm(body);
  }


  @Get('result')
  result() {
    return this.appService.showResult();
  }


}
