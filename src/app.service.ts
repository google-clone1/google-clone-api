import { Injectable } from '@nestjs/common';
import * as _ from 'lodash';

interface RESULT {
  name: string
  count: number
}
@Injectable()
export class AppService {

  googleFormList: RESULT[][] = [[], [], [], []]
  googleFormTotalRequest = 0

  getHello(): string {
    return 'Hello World!';
  }

  async submitForm(body: string[]) {
    this.googleFormTotalRequest += 1
    for (const [i, v] of body.entries()) {
      if (typeof v !== 'string') {
        break
      }
      const foundIndex = _.findIndex(this.googleFormList[i], { name: v })
      if (foundIndex < 0) {
        this.googleFormList[i].push({
          name: v,
          count: 1
        })
      } else {
        this.googleFormList[i][foundIndex].count += 1
      }
    }

    return { statusCode: 200, message: 'received successfully' }
  }


  showResult() {
    const resp = this.googleFormList.map(item => {
      const labels = item.map(i => i.name);
      const series = item.map(i => i.count);
      return { labels, series };
    });

    return {
      totalRequest: this.googleFormTotalRequest,
      graphList: resp
    }
  }



}
